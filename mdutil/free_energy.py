import numpy as np

def _pairwise_gauss(x,y=None,width=None,dist_mat=False):
	if width is None:
		print("Error: 'width' must be specified!")
		raise ValueError
	if dist_mat:
		if len(x.shape) != 3:
			print("Error: distance matrix must be 3-D!")
			raise ValueError
		mat=x
	else:
		if y is None:
			y=x
		mat=np.hstack(x[:,np.newaxis,i]-y[:,i] for i in range(x.shape[1]))
		if(x.shape[1]==1):
			mat=mat.reshape((x.shape[0],y.shape[0],1))
	return np.exp(-1.*np.sum(mat**2,axis=2)/(2.*width**2))

def _pairwise_Dgauss(x,y=None,width=None,dist_mat=False):
	if width is None:
		print("Error: 'width' must be specified!")
		raise ValueError
	if dist_mat:
		if len(x.shape) != 3:
			print("Error: distance matrix must be 3-D!")
			raise ValueError
		mat=x
	else:
		if y is None:
			y=x
		mat=np.hstack(x[:,np.newaxis,i]-y[:,i] for i in range(x.shape[1]))
		if(x.shape[1]==1):
			mat=mat.reshape((x.shape[0],y.shape[0],1))
	return -(mat/width**2)*_pairwise_gauss(mat,width=width,dist_mat=True)[:,:,np.newaxis]
	
	
class FreeEnergyReconstruction:
	default_width=1.
	def __init__(self,width=default_width,lamb=0.,relative=True,shift=0.,confidence_ratio=0.5):
		self.width=width
		self.lamb=lamb
		self.W=None
		self.relative=relative
		self.shift=shift
		self.code=""
		self.CR=confidence_ratio # E=CR*E_dU + (1-CR)*E_U
		if np.abs(self.CR)>1 or self.CR<0:
			print("Error: confidence ratio must be between 0 and 1")
		
	def fit(self,X,dU=None,U=None):
		self.X=X
		self.N_obs=X.shape[0]		
		if len(self.X.shape)==1:
			self.X=self.X.reshape((self.N_obs,1))
		self.N_dim=self.X.shape[1]
		self.dU=dU
		if dU is not None:
			self.code+="d"
			if(dU.shape[0] != self.N_obs):
				print("Error: X and Y must contain the same number of observations!")
				raise ValueError
			if len(self.dU.shape)==1:
				self.dU=self.dU.reshape((self.N_obs,1))
			if(self.dU.shape[1] != self.N_dim):
				print("Error: X and Y must have the same number of features!")
				raise ValueError	
			if self.relative:
				self.eps=1./(self.dU**2+self.shift)
			else:
				self.eps=np.ones((self.N_obs,self.N_dim))
		self.U=U
		if U is not None:
			self.code+="U"
			if(U.shape[0] != self.N_obs):
				print("Error: X and Y must contain the same number of observations!")
				raise ValueError
		if (U is None) and (dU is None):
			print("Error: At least U or dU must be provided!")
			raise ValueError
		self._compute_weights()
			
	def _compute_weights(self):
		if self.dU is not None:
			dPhi=_pairwise_Dgauss(self.X,width=self.width)
		if self.U is not None:
			Phi=_pairwise_gauss(self.X,width=self.width)
		if self.code[0]=="d":
			B=np.einsum('kil,kl,kjl->ij',dPhi,self.eps,dPhi)
			C=np.einsum('kil,kl,kl->i',dPhi,self.eps,self.dU)
			if self.U is not None:
				B=self.CR*B + (1-self.CR)*np.einsum('ki,kj->ij',Phi,Phi)
				C=self.CR*C + (1-self.CR)*np.einsum('ki,k->i',Phi,self.U)					
		else:
			B=np.einsum('ki,kj->ij',Phi,Phi)
			C=np.einsum('ki,k->i',Phi,self.U)
		self.W=np.linalg.solve(B+self.lamb*np.eye(self.N_obs), C)
	
	def predict_U(self,z):
		if self.W is None:
			print("Error: weights not computed yet!")
			raise ValueError
		U=_pairwise_gauss(z,self.X,width=self.width)
		return U.dot(self.W)
	
	def predict_dU(self,z):
		if self.W is None:
			print("Error: weights not computed yet!")
			raise ValueError
		U=_pairwise_Dgauss(z,self.X,width=self.width)
		return np.einsum('ikl,k->il',U,self.W)
	
	def fit_error(self):
		E=0
		if self.W is None:
			print("Error: weights not computed yet!")
			raise ValueError
		if self.dU is not None:
			E+=np.mean((self.predict_dU(self.X)-self.dU)**2)
		if self.U is not None:
			E+=np.mean((self.predict_U(self.X)-self.U)**2)
		return E

