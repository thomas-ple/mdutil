import numpy as np
import matplotlib.pyplot as plt

class Legends:
	def __init__(self,ax=None):
		self.legends=[]
		self.lines=[]
		self.kwargs={}
		if ax is None:
			self.ax=plt.gca()
		else:
			self.ax=ax
	
	def add(self,cntr,legend):
		line,_ = cntr.legend_elements()
		self.legends.append(legend)
		self.lines.append(line[0])
		self.update()
		
	def edit(self,**kwargs):
		self.kwargs.update(kwargs)
		self.update()
	
	def update(self):
		self.lgd=self.ax.legend(self.lines,self.legends,**self.kwargs)
	
	def reset(self):
		self.__init__()
		self.lgd=plt.legend()
		self.lgd.remove()
	
class ContourFile:
	def __init__(self,filename,order="C",label=None,**kwargs):
		self.filename=filename
		self.order=order
		if label is None:
			self.label=filename
		else:
			self.label=label
		self.plt_kwargs=kwargs
		self.get_grid()
	
	def add_contour(self,ax,levels,legend=None):
		if self.Z is None:
			self.get_grid()
		self.cntr=ax.contour(self.X,self.Y,self.Z,levels=levels,**self.plt_kwargs)
		if legend is not None:
			legend.add(self.cntr,self.label)
		
	def get_grid(self,Zcol=2):
		gnuplot_file=False
		with open(self.filename,"r") as f:
			if f.readline().split()[0] != "#":
				gnuplot_file=True
			
		if gnuplot_file:
			full_file=np.loadtxt(self.filename,usecols=(0,1,Zcol))
			self.X=np.array(list(sorted(set(full_file[:,0]))))
			self.Y=np.array(list(sorted(set(full_file[:,1]))))
			self.Z=np.reshape(full_file[:,Zcol], order=self.order
						,newshape=(self.X.shape[0],self.Y.shape[0],) )
		else:
			with open(self.filename,"r") as f:
				header_X=[float(a) for a in f.readline().split()[1:4]]
				header_P=[float(a) for a in f.readline().split()[1:4]]

			self.X=np.arange(header_X[0],header_X[1],(header_X[1]-header_X[0])/header_X[2])
			self.Y=np.arange(header_P[0],header_P[1],(header_P[1]-header_P[0])/header_P[2])
			
			self.Z=np.loadtxt(self.filename,skiprows=2).T
		
	
class ContourPlot:
	def __init__(self,ax=None,levels=5,xlim=None,ylim=None,xlabel="",ylabel="",title=None,fontsize=16):
		self.levels=levels
		self.title=title
		self.xlim=xlim
		self.ylim=ylim		
		self.xlabel=xlabel
		self.ylabel=ylabel
		self.files=[]
		self.fontsize=fontsize		
		if ax is None:
			self.ax=plt.gca()
		else:
			self.ax=ax		
		self.legend=Legends()
		self.configure_plot()
	
	def configure_plot(self):
		if self.title is not None:
			if isinstance(self.title,str):
				self.ax.set_title(self.title)
			else:
				self.ax.set_title(**self.title)
		if self.xlim is not None:
			self.ax.set_xlim(self.xlim)
		self.ax.set_xlabel(self.xlabel,fontsize=self.fontsize)
		if self.ylim is not None:
			self.ax.set_ylim(self.ylim)
		self.ax.set_ylabel(self.ylabel,fontsize=self.fontsize)		
	
	def add_file(self,cntr_file):
		self.files.append(cntr_file)
	
	def add_files(self,files):
		self.files.extend(files)
		
	def draw(self):
		for file in self.files:
			file.add_contour(ax=self.ax,levels=self.levels,legend=self.legend)
