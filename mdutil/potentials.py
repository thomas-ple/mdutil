import numpy as np
from numba import njit

from .atomic_units import au

@njit
def Morse(x,alpha=2.5*au.bohr,D=20./au.kcalperMol,xmax=2.5/au.bohr):
  aa=np.exp(-alpha*x)
  V  =  D*aa*(aa - 2.)
  dV = -D*2.*alpha*aa*(aa - 1.)
  sel=x>xmax
  V[sel] += D*np.exp(x[sel]-xmax)
  dV[sel] += D*np.exp(x[sel]-xmax)
  return V,dV

@njit
def Morse4(x,alpha=2.5*au.bohr,D=20./au.kcalperMol):
  aa=alpha*x
  aa2=aa*aa
  V = D*aa2*(1. - aa + 7.*aa2/12.)
  dV = D*alpha*aa*(2. - 3.*aa + 7.*aa2/3.)
  return V,dV

@njit
def Harmonic(x,k=0.1):  
  return 0.5*k*x**2,k*x