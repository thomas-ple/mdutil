from ensurepip import bootstrap
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.lines import Line2D
import sys
import re
from time import sleep as time_sleep 
from . import __DEFAULT_INTERVAL,__re_words,__re_float
from scipy.stats import gaussian_kde

__avg_color = "C1"
__data_color = "C0"
class PropertyNotFound(Exception):
  pass

def get_available_properties(tinkerfile=None,tinkerstr=None):
  if tinkerstr is not None:
    out=tinkerstr
  elif tinkerfile is not None:
    with open(tinkerfile,"r") as f: out=f.read()
  else:
    raise Exception("no data was provided")
  properties=set(
    re.findall("^ *("+__re_words+")[ =:]*"+__re_float,out,flags=re.I|re.M)
  )
  return properties

def print_available_properties(tinkerfile):
  properties = get_available_properties(tinkerfile)
  print(f"List of available properties in '{tinkerfile}':")
  for prop in properties:
    print(f"- {prop}")

def tinkerio_property(prop,tinkerfile=None,tinkerstr=None):
  if tinkerstr is not None:
    out=tinkerstr
  elif tinkerfile is not None:
    with open(tinkerfile,"r") as f: out=f.read()
  else:
    raise Exception("no data was provided")
  data=np.array(
    re.findall("^ *"+prop+"[ =:]*("+__re_float+")",out,flags=re.I|re.M)
    ,dtype="float"
  )
  if len(data)>0: return data
  raise PropertyNotFound(f"Property '{prop}' was not found.")

def tinkerio_properties(props,tinkerfile=None,tinkerstr=None):
  data=[]
  for prop in props:
    data.append(tinkerio_property(prop,tinkerfile,tinkerstr))
  return np.array(data).T

def generate_properties(props,tinkerfile,sleep=time_sleep,get_simulation_time=False,watch=False,**kwargs):
  pf=open(tinkerfile,"r")
  #pf.seek(0,os.SEEK_END)
  outstr=""
  notfound=0
  propsfull=[prop.lower() for prop in props]
  interval = kwargs["interval"] if kwargs["interval"] else __DEFAULT_INTERVAL
  if get_simulation_time:
    if not watch:
      try:
        time = tinkerio_property("simulation time",tinkerfile)
      except PropertyNotFound:
        get_simulation_time=False

    if "simulation time" not in propsfull:
      propsfull+=["simulation time"]
      indextime=-1
    else:
      indextime=propsfull.index("simulation time")
  data=np.empty((0,len(propsfull)))
  while True:
    #newline=pf.readline()
    outstr=pf.read()
    if not outstr:
      sleep(interval)
      continue
    try:
      newdat = tinkerio_properties(propsfull,tinkerstr=outstr)
      data = np.concatenate((data,newdat))
      notfound=0
      if get_simulation_time:
        yield data[:,:-1],data[:,indextime]
      else:
        yield data, np.arange(data.shape[0])
    except PropertyNotFound as err:
      if outstr:
        notfound+=1
        if not watch: 
          raise err
      if notfound>10:
        print(err)
    except GeneratorExit:
      print(f"closing {tinkerfile}")
      pf.close()
      return
    except KeyboardInterrupt:
      print(f"closing {tinkerfile}")
      pf.close()
      sys.exit(0)
    except Exception as err:
      print(err)


def cumavg(data):
  return np.cumsum(data,axis=0)/(np.arange(data.shape[0])+1.)[:,None]

def cleanup_ax(ax):
  ax.set_prop_cycle(None)
  for line in ax.get_children():
    try:
      line.remove()
    except:
      pass

def setup_figures(props,exit_when_closed=False,**kwargs):
  #plt.ion()
  matplotlib.rcParams['figure.raise_window'] = False
  for i,prop in enumerate(props):
    fig=plt.figure(i)
    if exit_when_closed: 
      fig.canvas.mpl_connect('close_event',lambda e: sys.exit(0))
    plt.title(prop)
    plt.grid(True)

def properties_header(props,get_simulation_time=False):
  props_out = ["_".join(prop.split()) for prop in props] 
  header = " ".join(props_out)
  return "time "+header if get_simulation_time else "frame "+header

def bootstrap_analysis(data,n_iter=1000,return_samples=False):
  bootstrap_samples = np.empty(n_iter)
  n=len(data)
  for iter in range(n_iter):
    bootstrap_samples[iter]=np.random.choice(data,size=n,replace=True).mean()
  if return_samples: return bootstrap_samples
  return bootstrap_samples.mean(),bootstrap_samples.std()

def blocking_std(data,mode="weighted",ax=None):
  n=len(data)
  if n<2: return np.NaN
  blockdat=np.copy(data)
  mean=data.mean()
  std=np.std(blockdat)/np.sqrt(n-1)
  stds=[]
  stderrs=[]
  stds.append(std)
  stderrs.append(std/np.sqrt(2*(n-1)))
  while True:
    blockdat = 0.5*(blockdat[:-1:2]+blockdat[1::2])
    n=len(blockdat)
    if n<2: break
    std=np.std(blockdat)/np.sqrt(n-1)
    stds.append(std)
    stderrs.append(std/np.sqrt(2*(n-1)))
  stds=np.array(stds)
  stderrs=np.array(stderrs)
  imax=np.argmax(stds)
  stdmax=stds[imax]
  imaxmin=np.argmax(stds-stderrs)
  stdmaxmin=stds[imaxmin]
  if mode=="naive":
    std = stds[0]
  elif mode=="max":
    std = stdmax
  elif mode=="maxmin":
    std = stdmaxmin
  elif mode=="weighted":
    wmax=1./(stderrs[imax]+1.e-8)
    wmaxmin=1/(stderrs[imaxmin]+1.e-8)
    std=(stdmax*wmax + stdmaxmin*wmaxmin)/(wmax+wmaxmin)
  else:
    raise Exception(f"Blocking mode {mode} not recognized")
  #std=0.5*(np.max(stds)+stds[np.argmax(stds-stderrs)])
  if ax is not None:
    ax.plot(stds)
    ax.fill_between(np.arange(len(stds)),stds-stderrs,stds+stderrs,alpha=0.5)
    ax.axvline(np.argmax(stds),color="red")
    ax.axvline(np.argmax(stds-stderrs),color="green")
    ax.axhline(std,color="k")
  #print(np.max(stds),stds[np.argmax(stds-stderrs)])
  return std

  

def plot_properties(props,tinkerfile,thermalize=0.,get_simulation_time=False,watch=False,histogram=False,outfile=None,verbose=False,**kwargs):
  nup=0
  if watch: print(f"Started streaming {props} from '{tinkerfile}'")
  setup_figures(props,watch)
  xlabel = "simulation time (ps)" if get_simulation_time else "frame number"
  writeout = outfile is not None
  header = properties_header(props,get_simulation_time)
  relshift= 1.02
  bw=kwargs["bw"] if "bw" in kwargs else None
  thermalize=max(0,thermalize)
  for data,time in generate_properties(props,tinkerfile,plt.pause,get_simulation_time,watch=watch,**kwargs):
    nup+=1

    if thermalize>0:
      avg = np.concatenate((data[time<thermalize,:]
                      ,cumavg(data[time>=thermalize,:])))
    else:
      avg=cumavg(data)
    nsample=np.count_nonzero(time>=thermalize)

    if verbose:
      if watch: print("update ",nup,end=", ")
      print("Sample size ",nsample)
    if writeout: np.savetxt(outfile,np.column_stack((time,data))
                    ,header=header
                    ,footer = "avg= "+" ".join([str(a) for a in avg[-1,:]])
                  )
    if nsample>2:
      print(f'{"Property":>20} {"Average":>12}      2sig(95%)     1/Deff   Neff(Ns={nsample})')
    for i,prop in enumerate(props):
      plt.figure(i)
      #plt.clf()
      if nup >1: cleanup_ax(plt.gca())
      plt.plot(time,data[:,i],label="data",color=__data_color)
      plt.plot(time,avg[:,i],label="cumulative avg",color=__avg_color)
      plt.xlabel(xlabel)

      histdat = data[time>=thermalize,i]
      if verbose and nsample<=2: print(prop,avg[-1,i])
      if nsample>2:
        std_naive = np.std(histdat)/np.sqrt(nsample-1)
        #plt.figure(len(props)+i)
        #plt.title("blocking "+prop)
        #ax=plt.gca()
        #if nup >1: cleanup_ax(ax)
        std=blocking_std(histdat)
        do_hist = std/abs(avg[-1,i])>1.e-5
        #plt.figure(i)
        eff_sampling = (std_naive/std)**2 if do_hist else 1.
        if verbose:
          avg_str=f'{avg[-1,i]:12.5f} +/- {2*std:<12.5f}'
          ratio_str=f'{int(nsample*eff_sampling)}/{nsample}'
          print(f'{prop:>20} {avg_str}   {100*eff_sampling:5.1f}%    {int(nsample*eff_sampling)}')
        timetherm=time[time>=thermalize]
        avgtherm=avg[time>=thermalize,i]
        nstd=min(100,nsample)
        stds=np.zeros(nstd)
        js = np.linspace(2,nsample,nstd,dtype="int")-1
        for k,j in enumerate(js):
          stds[k]=blocking_std(histdat[:j])
        plt.fill_between(timetherm[js],avgtherm[js]-2*stds,avgtherm[js]+2*stds,linewidth=0,color=__avg_color,alpha=.4,zorder=10)
        if histogram and do_hist:
          centers=np.linspace(avg[-1,i]-5*std,avg[-1,i]+5*std,100)
          dens_avg=np.exp(-(centers-avg[-1,i])**2/(2*std**2))
          x=time[-1]*(relshift+0.1*dens_avg)
          plt.fill_betweenx(centers,time[-1]*relshift,x,color=__avg_color,alpha=0.4,linewidth=0
                            ,label=f"avg posterior (blocking gaussian)",zorder=10)

      if histogram and nsample>2 and do_hist:
        #hist,edges=np.histogram(histdat,bins=100)
        #centers=0.5*(edges[:-1]+edges[1:])
        #plt.plot(time[-1]+hist,centers,label="histogram",color=__data_color)
        std=np.std(histdat)
        centers=np.linspace(avg[-1,i]-3*std,avg[-1,i]+3*std,100)
        #centers=np.linspace(np.min(histdat),np.max(histdat),100)
        kde=gaussian_kde(histdat,bw_method=bw)
        dens=kde(centers)
        avghist = np.average(centers,weights=dens)
        maxlikelihood = centers[np.argmax(dens)]
        avgerr=np.abs(avghist-maxlikelihood)
        x=time[-1]*(relshift+0.1*dens/np.max(dens))

        #plt.plot(x,centers,label=f"density (KDE bw={bw})",linewidth=3,alpha=0.6)
        plt.fill_betweenx(centers,time[-1]*relshift,x,linewidth=0,alpha=0.6,color=__data_color
                          ,label=f"data density (KDE bw={bw})")
        #plt.axvline(time[-1]*relshift,color="k",alpha=0.2)
        #plt.fill_between(time,avghist-avgerr,avghist+avgerr,color=__avg_color,alpha=0.2)
        #plt.axhline(avghist,color=__avg_color,alpha=0.5,label=f"avg from density")
      
      #plt.legend() #bbox_to_anchor=(0, 1.05, 1, 0), loc="lower left", mode="expand", ncol=2)
      #plt.tight_layout()

    plt.show(block=not watch)

    if not watch:
      return data,time
    elif verbose:
      print()

def plot_histogram(props,tinkerfile,thermalize=0.,get_simulation_time=False,watch=False,verbose=False,**kwargs):
  nup=0
  if watch: print(f"Started streaming histograms of {props} from '{tinkerfile}'")
  setup_figures(props,watch)
  for data,time in generate_properties(props,tinkerfile,plt.pause,get_simulation_time,watch=watch,**kwargs):
    nup+=1
    if verbose and watch: print("update ",nup)
    if(thermalize>0): data = data[time>=thermalize]

    if data.shape[0]<2:
      if watch: continue
      print("not enough data to make histogram")
      return data,None
    for i,prop in enumerate(props):
      plt.figure(i)
      #plt.clf()
      if nup >1:
        cleanup_ax(plt.gca())
      avg = np.mean(data[:,i])  
      std=blocking_std(data[:,i])
      if verbose: print(prop,avg,'+/-',2*std)
      #std=np.std(data[:,i]-avg) 
      #hist,edges=np.histogram(data[:,i],bins=100,density=True)
      #centers=0.5*(edges[:-1]+edges[1:])
      #plt.plot(centers,hist,label="histogram")
      centers=np.linspace(np.min(data[:,i]),np.max(data[:,i]),100)
      centers_avg=np.linspace(avg-5*std,avg+5*std,100)
      nbins=kwargs["nbins"] if kwargs["nbins"] else 30
      bw=kwargs["bw"] if kwargs["bw"] else None
      kde=gaussian_kde(data[:,i],bw_method=bw)
      dens=kde(centers)
      dens_avg=np.exp(-(centers_avg-avg)**2/(2*std**2))*max(dens)
      plt.hist(data[:,i],bins=nbins,density=True,label="histogram",color=__data_color,alpha=0.6)
      plt.plot(centers,dens,label=f"data density (KDE bw={bw})",linewidth=3,color=__data_color)
      plt.fill_between(centers_avg,np.zeros_like(dens_avg),dens_avg,label=f"avg posterior (blocking gaussian, scaled)",color=__avg_color,alpha=0.5)
      plt.axvline(avg,color=__avg_color)
      plt.legend(bbox_to_anchor=(0, 1.05, 1, 0), loc="lower left", mode="expand", ncol=2)
      plt.tight_layout()
    
    plt.show(block=not watch)
    if not watch:
      return data,kde
    elif verbose:
      print()






