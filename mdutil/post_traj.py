import numpy as np
import numba

class Histogram:
	def __init__(self,histo,xcenters,ycenters=None):
		if histo.shape[0] != xcenters.shape[0]:
			print("Error: sizes do not match!")
			raise TypeError

		self.values=histo
		self.xcenters=xcenters
		if ycenters is None:
			self.ycenters=None
			self.dim=1
		else:
			self.ycenters=ycenters
			self.dim=2
			if histo.shape[1] != ycenters.shape[0]:
				print("Error: sizes do not match!")
				raise TypeError
	
	def write_for_gnuplot(self,filename):
		if self.dim == 2:
			write_density2D_for_gnuplot(filename,self.values,self.xcenters,self.ycenters)
		else:
			np.savetxt(filename,np.column_stack((self.xcenters,self.values))
						,fmt="%.5e",delimiter="  ")

#----------------------------------------------------

def density_from_file(file1,rank=1,file2=None,rank2=1,nbins=100
						,weightfile=None,rankweight=1
						,xrange=None,yrange=None
						,binary=False,ncol=1,binary2=False,ncol2=1,normed=True):
#compute a density from traj1 and traj2 contained in file1 and file2
# in columns rank and rank2
# and returns a Histogram object

	traj1=get_traj_from_file(file1,ncol=ncol,binary=binary)

	if(weightfile is not None):
		weights=np.loadtxt(weightfile)

	if file2 is not None:
		traj2=get_traj_from_file(file2,ncol=ncol2,binary=binary2)
		if(weightfile is not None):
			return density_from_traj(traj1[:,rank],traj2[:,rank2]
				,nbins=nbins,weights=weights[:,rankweight]
				,xrange=xrange,yrange=yrange,normed=normed)
		else:
			return density_from_traj(traj1[:,rank],traj2[:,rank2]
				,nbins=nbins,xrange=xrange,yrange=yrange,normed=normed)
	else:
		if(weightfile is not None):
			return density_from_traj(traj1[:,rank]
				,nbins=nbins,weights=weights[:,rankweight]
				,xrange=xrange,normed=normed)
		else:
			return density_from_traj(traj1[:,rank]
				,nbins=nbins,xrange=xrange,normed=normed)
	
def density_from_traj(traj1,traj2=None,nbins=100,weights=None
					,xrange=None,yrange=None,normed=True):
#compute a density from traj1 and traj2 and returns a Histogram object
	if traj2 is None:
		if weights is None:
			histogram,xbins=np.histogram(traj1,nbins,density=normed,range=xrange)
		else:
			if weights.size != traj1.size:
				print("Error: weights and traj must be the same length.")
				raise LengthError
			histogram,xbins=np.histogram(traj1,nbins,density=normed,weights=weights,range=xrange)

		xcenters=(xbins[:-1]+xbins[1:])/2
		return Histogram(histogram,xcenters)

	else:
		if traj2.size != traj1.size:
			print("Error: traj1 and traj2 must be the same length.")
			raise LengthError
		
		if xrange is None:
			xrange=(traj1.min(),traj1.max())
		if yrange is None:
			yrange=(traj2.min(),traj2.max())
		rng=np.array((np.array(xrange),np.array(yrange)))
			

		if weights is None:
			histogram,xbins,ybins=np.histogram2d(traj1,traj2,nbins,normed=normed,range=rng)
		else:
			if weights.size != traj1.size:
				print("Error: weights and traj must be the same length.")
				raise LengthError
			histogram,xbins,ybins=np.histogram2d(traj1,traj2,nbins
				,normed=normed,weights=weights,range=rng)

		xcenters=(xbins[:-1]+xbins[1:])/2
		ycenters=(ybins[:-1]+ybins[1:])/2
		return Histogram(histogram,xcenters,ycenters)

def write_density2D_for_gnuplot(filename,histogram,xcenters,ycenters):
#write a 2D density in gnuplot format for splot (with blank lines)
	nx=int(xcenters.size)
	ny=int(ycenters.size)
	array=np.zeros((nx,3))

	f=open(filename,"w")
	for i in range(ny):
		array=np.column_stack((xcenters,
				np.full(nx,ycenters[i]),
				histogram[:,i]))
		#array.tofile(f,"  ")
		np.savetxt(f,array,fmt="%.5e",delimiter="  ")
		f.write('\n')	
	f.close()

def mean_density_from_trajs(trajs1,trajs2=None,nbins=100,weights=None
					,xrange=None,yrange=None,normed=True):
	
	if xrange is None:
		xrange=find_min_max(trajs1)
	
	ntraj=len(trajs1)
	if weights is not None:
		if len(weights) != ntraj:
			print('Error: trajs1 and weights must be the same length.')
			raise LengthError
	
	densities=[]
	if trajs2 is None:
		for i in range(ntraj):
			if weights is not None:
				weight=weights[i]
			else:
				weight=None
			densities.append(density_from_traj(trajs1[i],nbins=nbins
				,weights=weight,xrange=xrange,normed=normed))
	else:
		if len(trajs2) != ntraj:
			print('Error: trajs1 and trajs2 must be the same length.')
			raise LengthError

		if yrange is None:
			yrange=find_min_max(trajs2)

		for i in range(ntraj):
			if weights is not None:
				weight=weights[i]
			else:
				weight=None
			densities.append(density_from_traj(trajs1[i],trajs2[i],nbins=nbins
				,weights=weight,xrange=xrange,yrange=yrange,normed=normed))

	xcenters=densities[0].xcenters
	ycenters=densities[0].ycenters
	mean_density=np.float64(densities[0].values)/ntraj
	for i in range(1,ntraj):
		mean_density+=np.float64(densities[i].values)/ntraj
	#mean_density/=ntraj

	return Histogram(mean_density,xcenters,ycenters)

def find_min_max(trajs):
	minvals=[]
	maxvals=[]
	for traj in trajs:
		minvals.append(traj.min())
		maxvals.append(traj.max())
	return (min(minvals),max(maxvals))

#----------------------------------------------------

class Spectrum:
	def __init__(self,spec,dt,domega,imag=None):
		#all values must be in a.u.
		self.values=np.copy(spec)
	#	self.imag=imag
		self.oldvalues=spec
		self.original_values=spec
		self.dt=dt
		self.domega=domega
		self.n=spec.size
		#self.time=np.linspace(0,(self.n-1)*dt,self.n)
		self.time=self.n*self.dt*np.fft.fftfreq(self.n)
		#self.omega=np.linspace(-(self.n-2)*domega/2,(self.n-1)*domega/2,self.n)
		self.omega=self.n*self.domega*np.fft.fftfreq(self.n)
		# self.mid=int(self.n/2)
		# tmp=np.copy(self.omega[:self.mid])
		# self.omega[:self.mid]=np.copy(self.omega[self.mid:])
		# self.omega[self.mid:]=np.copy(tmp[:])
	
	def get_tcf(self):
		Ct=np.fft.fftshift(np.fft.fft(self.values))/(self.n*self.dt)
		return np.column_stack((np.fft.fftshift(self.time),np.real(Ct),np.imag(Ct)))
	
	def copy(self):
		return Spectrum(np.copy(self.values),self.dt,self.domega)
	
	def smooth(self,gsmooth):
	#smooths a spectrum by gaussian filtering in time
	#gsmooth and domega must be in the same unit
		if gsmooth<=0 :
			return
		self.oldvalues=np.copy(self.values)
		#Ct=np.fft.fft(self.values,norm="ortho")
		Ct=np.fft.fft(self.values)
		Ct*=np.exp(-(gsmooth*self.time)**2)
		#Ct[0]*=0.5
		#self.values=np.roll(np.real(np.fft.fft(Ct,norm="ortho"))[::-1],1)
		self.values=np.fft.ifft(Ct)
	
	def restore_previous_spectrum(self):
		if self.oldvalues is not None:
			self.values=np.copy(self.oldvalues)
			self.oldvalues=None
		else:
			print("Warning: no previous spectrum to recover.")
	
	def restore_original_spectrum(self):
		self.oldvalues=np.copy(self.values)
		self.values=np.copy(self.original_values)
	
	def write_to_file(self,filename,unit=1.,cutoff=-1.,imag=False,negative_freq=True):
	#write the spectrum to a file using 'unit' to rescale
	#the omega axis and cuts at 'cutoff' (in the same unit as 'unit')
		if cutoff <= 0:
			lim=int(self.n/2)
		else:
			lim=int(cutoff/(self.domega*unit))
		with open(filename,'wb') as f:
			if imag:
				if negative_freq:
					np.savetxt(f,np.column_stack((self.omega[-lim:]*unit
												,np.real(self.values[-lim:])
												,np.imag(self.values[-lim:]))))
				np.savetxt(f,np.column_stack((self.omega[:lim]*unit
												,np.real(self.values[:lim])
												,np.imag(self.values[:lim]))))
			else:
				if negative_freq:
					np.savetxt(f,np.column_stack((self.omega[-lim:]*unit,np.real(self.values[-lim:]))))
				np.savetxt(f,np.column_stack((self.omega[:lim]*unit,np.real(self.values[:lim]))))
	
	def write_tcf(self,filename,unit=1.,cutoff=-1.,imag=False,negative_time=False):
		if cutoff <= 0:
			lim=int(self.n/2)
		else:
			lim=int(cutoff/(self.dt*unit))
		Ct=np.fft.fft(self.values)/(self.n*self.dt)
		with open(filename,'wb') as f:
			if imag:
				if negative_time:
					np.savetxt(f,np.column_stack((self.time[-lim:]*unit
												,np.real(Ct[-lim:])
												,np.imag(Ct[-lim:])
											)))
				np.savetxt(f,np.column_stack((self.time[:lim]*unit
												,np.real(Ct[:lim])
												,np.imag(Ct[:lim])
											)))
			else:
				if negative_time:
					np.savetxt(f,np.column_stack((self.time[-lim:]*unit,np.real(Ct[-lim:]))))
				np.savetxt(f,np.column_stack((self.time[:lim]*unit,np.real(Ct[:lim]))))
	
	def inverse_piqtb_c(self,temperature,nu):
		"""
			transform back a piqtb-c correlation function
			into a standard correlation function
			temperature must be provided in kelvin
			domega in atomic units
			(hbar=1)
		"""
		self.oldvalues=np.copy(self.values)

		kT=temperature/3.15774e5
		om=np.abs(self.omega)/kT

		omegap = nu*kT
		omegap2= omegap*omegap

		kernel=0.5*om[1:]/np.tanh(0.5*om[1:])
		if nu > 1:
			for i in range(1,nu):
				kernel -= self.omega[1:]**2/(self.omega[1:]**2+4*omegap2*(np.sin(i*np.pi/nu))**2)

		self.values[1:]=self.oldvalues[1:]/kernel
		#self.inverse_kubo(temperature)

	def sym2kubo(self,temperature):
		self.oldvalues=np.copy(self.values)

		kT=temperature/3.15774e5
		om=np.abs(self.omega)/kT

		kernel=0.5*om[1:]/np.tanh(0.5*om[1:])

		self.values[1:]=self.oldvalues[1:]/kernel
		#self.inverse_kubo(temperature)
	
	def kubo2sym(self,temperature):
		self.oldvalues=np.copy(self.values)

		kT=temperature/3.15774e5
		om=np.abs(self.omega)/kT

		kernel=0.5*om[1:]/np.tanh(0.5*om[1:])

		self.values[1:]=self.oldvalues[1:]*kernel
		#self.inverse_kubo(temperature)
	
	def kubo2std(self,temperature):
		self.oldvalues=np.copy(self.values)

		kT=temperature/3.15774e5		

		#self.values[0]=0.
		om=self.omega/kT
		sel=om > -5.e+02
		sel[0]=False #do not treat zero frequency term
		self.values[sel]=self.oldvalues[sel]*om[sel]/(1-np.exp(-1*om[sel]))
		self.values[~sel]=-1.*self.oldvalues[~sel]*om[~sel]*np.exp(om[~sel])

		self.values[0]=self.oldvalues[0]
	
	def std2kubo(self,temperature):
		self.oldvalues=np.copy(self.values)

		kT=temperature/3.15774e5	

		#self.values[0]=0.
		om=self.omega/kT
		sel=om > -5.e+02
		sel[0]=False #do not treat zero frequency term
		self.values[sel]=self.oldvalues[sel]/om[sel]*(1-np.exp(-1*om[sel]))
		self.values[~sel]=-1.*self.oldvalues[~sel]/(om[~sel]*np.exp(om[~sel]))

		self.values[0]=self.oldvalues[0]
	
	def sym2std(self,temperature):
		self.sym2kubo(temperature)
		self.kubo2std(temperature)
	
	def std2sym(self,temperature):
		self.std2kubo(temperature)
		self.kubo2sym(temperature)
		
	
	def inverse_piqtb_st(self,temperature,nu):
		"""
			transform back a piqtb-st correlation function
			into a standard correlation function
			temperature must be provided in kelvin
			domega in atomic units
			(hbar=1)
		"""
		self.oldvalues=np.copy(self.values)

		kT=temperature/3.15774e5
		om=np.abs(self.omega[1:])

		omegap = nu*kT
		omegap2= omegap*omegap

		bead_contrib=np.zeros_like(om)
		for k in range(nu):
			omk2=om**2 + 4*omegap2*(np.sin(k*np.pi/nu))**2
			bead_contrib += 1./(nu*omk2)
		
		kernelinv=2.*om*np.tanh(0.5*om/kT)*bead_contrib

		self.values[1:]=self.oldvalues[1:]*kernelinv*nu*kT
		#self.inverse_kubo(temperature)

	def inverse_kubo(self,temperature):
		"""
			transform back a kubo correlation function
			into a standard correlation function
			temperature must be provided in kelvin
			domega in atomic units
			(hbar=1)
		"""
		self.oldvalues=np.copy(self.values)

		kT=temperature/3.15774e5
		

		#self.values[0]=0.
		om=self.omega/kT
		sel=om > -5.e+02
		sel[0]=False #do not treat zero frequency term
		self.values[sel]=self.oldvalues[sel]*om[sel]/(1-np.exp(-1*om[sel]))
		self.values[~sel]=-1.*self.oldvalues[~sel]*om[~sel]*np.exp(om[~sel])

		self.values[0]=self.oldvalues[0]
		# print(-1*self.omega[mid:]/kT)
		#print(np.sinh(-1.e+02))
		#self.values[:]=self.oldvalues[:]*self.omega[:]/(2*kT*np.tanh(self.omega[:]/(2*kT)))

	def rescale_time(self,scale_factor):
		self.dt*=scale_factor
		self.time*=scale_factor
		self.values*=scale_factor
		self.domega/=scale_factor
		self.omega/=scale_factor
		if self.oldvalues is not None:
			self.oldvalues*=scale_factor
	
	def deconvolute(self,gamma,niteration=10,every=1,verbose=False
									,qtb=False,temperature=300.,fftshift=True,cutoff=None):
		self.oldvalues=np.copy(self.values)
		if every > 1:
			self.domega*=every
			self.values=np.copy(self.values[np.arange(0,self.n,every)])
			self.omega=np.copy(self.omega[np.arange(0,self.n,every)])
			self.n=self.values.shape[0]
		omega=np.fft.ifftshift(self.omega)
		if fftshift:
			spectrum=np.fft.ifftshift(self.values)
		else:
			spectrum=np.copy(self.values)			
		spectrum=np.real(spectrum)
		if cutoff is not None:
			sel=np.abs(omega)<=cutoff
			spectrum=spectrum[sel]
			omega=omega[sel]
		else:
			sel=np.abs(omega)>=0
		nom=len(omega)
		print("nom=",nom)
		D=_compute_D(omega,omega,nom,nom
					,self.domega,gamma,qtb,verbose=verbose,temperature=temperature)
		#	np.savetxt("D_reconstruction_"+str(gamma)+".tmp",D)
		K=_build_K(omega,nom,self.domega,gamma,qtb,verbose=verbose,temperature=temperature)
		h=_compute_h(spectrum,omega,nom,self.domega,K)

		f,rn,ln=_iterate_deconvolution(spectrum,D,h,K,niteration
				,omega,nom,self.domega,thr=1e-4,verbose=verbose)
		
		
		if fftshift:
			spectrum=np.fft.ifftshift(self.values)
		else:
			spectrum=np.copy(self.values)
		spectrum[sel]=f
		self.spectrum_reconv=np.copy(spectrum)
		self.spectrum_reconv[sel]=_compute_h(f,omega,nom,self.domega,K)
		self.spectrum_reconv=np.fft.fftshift(self.spectrum_reconv)
		self.values=np.fft.fftshift(spectrum)
		self.rn_deconvolution=rn
		self.ln_deconvolution=rn
		

def spectrum_from_traj(traj1,traj2=None,dt=1.,padding_mult=2,unit=1.,unit2=1.,n_steps=None):
#compute the Fourier transform of the time cross-correlation
#function of traj1 and traj2 
# (or autocorrelation if only traj1 is provided)
#dt must be in a.u.
	if n_steps is None:
		n_steps=traj1.size
	else:
		if n_steps > traj1.size:
			print("Error: n_steps greater than traj1.size !")
			raise LengthError
	n=int(padding_mult*n_steps)
	domega=2*np.pi/(n*dt)
	traj1/=unit
	ft1=np.fft.ifft(traj1[:n_steps],n,norm="ortho")
	if traj2 is not None:
		if traj2.size < n_steps:
			print("Error: traj2.size cannot be lower than n_steps !")
			raise LengthError
		traj2/=unit2
		ft2=np.fft.ifft(traj2[:n_steps],n,norm="ortho")
		#CC=np.real(ft1)*np.real(ft2)+np.imag(ft1)*np.imag(ft2)
		#iCC=np.imag(ft1)*np.real(ft2)-np.imag(ft2)*np.real(ft1)
		#tmp=ft1*ft2[::-1]
		#CC=np.real(tmp)
		#iCC=np.imag(tmp)
		CC=ft1*np.conj(ft2)
	else:
		#CC=np.real(ft1)*np.real(ft1)+np.imag(ft1)*np.imag(ft1)
		#iCC=np.zeros(n)
		CC=ft1*np.conj(ft1)
	
	CC*=max(1,padding_mult)*dt
	#iCC*=max(1,padding_mult)*dt
	return Spectrum(CC,dt,domega)

def spectrum_from_file(file1,rank=0,file2=None,rank2=0
						,dt=1.,unit=1.,unit2=1.
						,binary=False,ncol=1,binary2=False,ncol2=1,n_steps=None):
#compute the Fourier transform of the time cross-correlation
#function of traj1 and traj2 contained in file1 and file2
# in columns rank and rank2
# (or autocorrelation if only traj1 is provided)
#dt must be in a.u.

	traj1=get_traj_from_file(file1,ncol=ncol,binary=binary)

	if file2 is not None:
		traj2=get_traj_from_file(file2,ncol=ncol2,binary=binary2)
		#print(traj1.shape[0],traj2.shape[0])
		return spectrum_from_traj(traj1[:,rank],traj2[:,rank2]
				,dt=dt,unit=unit,unit2=unit2,n_steps=n_steps)
	else:
		return spectrum_from_traj(traj1[:,rank]
				,dt=dt,unit=unit,unit2=unit2,n_steps=n_steps)	

def mean_spectrum_from_trajs(trajs1,trajs2=None,dt=1.,padding_mult=2,unit=1.,unit2=1.,n_steps=None):
	if n_steps is None:
		n_steps=trajs1[0].size
		for traj in trajs1:
			if traj.size != n_steps:
				print("Error: All the trajectories must be the same length.")
				raise LengthError
	spectra=[]
	n_trajs=len(trajs1)
	if trajs2 is None:
		for traj in trajs1:
			spectrum_loc=spectrum_from_traj(traj,dt=dt,padding_mult=padding_mult,unit=unit,n_steps=n_steps)
			spectra.append(spectrum_loc)
	else:
		if n_trajs != len(trajs2):
			print("Error: trajs1 and trajs2 must contain the same number of trajectories.")
			raise TypeError
		for i in range(n_trajs):
			spectrum_loc=spectrum_from_traj(trajs1[i],trajs2[i]
							,dt=dt,padding_mult=padding_mult,unit=unit,unit2=unit2,n_steps=n_steps)
			spectra.append(spectrum_loc)
	
	for spectrum in spectra[1:]:
		spectra[0].values+=spectrum.values
	spectra[0].values/=n_trajs
	return spectra[0]

def mean_spectrum_from_files(files1,rank=0,files2=None,rank2=0
						,dt=1.,unit=1.,unit2=1.
						,binary=False,ncol=1,binary2=False,ncol2=1,n_steps=None):
	n_trajs=len(files1)
	#print(n_trajs,"trajectories.")
	if files2 is not None:
		if len(files2) != n_trajs:
			print("Error: files1 and files2 must contain the same number of trajectories.")
			raise TypeError	
	else:
		files2=[None for i in range(n_trajs)]

	spectrum=spectrum_from_file(files1[0],rank=rank,file2=files2[0],rank2=rank2
						,dt=dt,unit=unit,unit2=unit2
						,binary=binary,ncol=ncol,binary2=binary2,ncol2=ncol2,n_steps=n_steps)
	if n_trajs==1:
		return spectrum
	spectrum.values/=n_trajs
	for i in range(1,n_trajs):
		spectrum.values+=spectrum_from_file(files1[i],rank=rank,file2=files2[i],rank2=rank2
						,dt=dt,unit=unit,unit2=unit2
						,binary=binary,ncol=ncol,binary2=binary2,ncol2=ncol2,n_steps=n_steps).values/n_trajs
	return spectrum
	


def write_spectra_to_file(spectra,filename,unit=1.,cutoff=-1.,imag=False,negative_freq=True):
	n=spectra[0].n
	domega=spectra[0].domega
	dt=spectra[0].dt

	if cutoff <= 0:
		lim=int(n/2)
	else:
		lim=int(cutoff/(domega*unit))
	
	file_array=spectra[0].omega[:lim]*unit
	file_array_neg=spectra[0].omega[-lim:]*unit
	if imag:
		file_array_imag=spectra[0].omega[:lim]*unit
		file_array_neg_imag=spectra[0].omega[-lim:]*unit
	#print(file_array_neg)

	for spectrum in spectra:
		if (spectrum.n != n) or (spectrum.domega != domega) or (spectrum.dt != dt):
			print("All spectra must have the same characteristics!")
			raise LengthError
		file_array=np.column_stack( (file_array, np.real(spectrum.values[:lim])))
		file_array_neg=np.column_stack( (file_array_neg, np.real(spectrum.values[-lim:])) )
		if imag:
			file_array_imag=np.column_stack( (file_array_imag, np.imag(spectrum.values[:lim])))
			file_array_neg_imag=np.column_stack( (file_array_neg_imag, np.imag(spectrum.values[-lim:])) )
	
	if imag:
		with open(filename+".real",'wb') as f:
			if negative_freq:
				np.savetxt(f,file_array_neg)
			np.savetxt(f,file_array)
		with open(filename+".imag",'wb') as f:
			if negative_freq:
				np.savetxt(f,file_array_neg_imag)
			np.savetxt(f,file_array_imag)
	else:
		with open(filename,'wb') as f:
			if negative_freq:
				np.savetxt(f,file_array_neg)
			np.savetxt(f,file_array)

def write_all_tcf(spectra,filename,unit=1.,cutoff=-1.,imag=False):
	n=spectra[0].n
	domega=spectra[0].domega
	dt=spectra[0].dt

	if cutoff <= 0:
		lim=int(n/2)
	else:
		lim=int(cutoff/(dt*unit))
	
	file_array=spectra[0].time[:lim]*unit
	file_array_imag=spectra[0].time[:lim]*unit
	#print(file_array_neg)

	for spectrum in spectra:
		if (spectrum.n != n) or (spectrum.domega != domega) or (spectrum.dt != dt):
			print("All spectra must have the same characteristics!")
			raise LengthError
		Ct=np.fft.fft(spectrum.values)[:lim]/(n*dt)
		file_array=np.column_stack((file_array, np.real(Ct)))
		file_array_imag=np.column_stack((file_array_imag, np.imag(Ct)))
	
	if imag:
		with open(filename+".real",'wb') as f:
			np.savetxt(f,file_array)
		with open(filename+".imag",'wb') as f:
			np.savetxt(f,file_array_imag)
	else:
		with open(filename,'wb') as f:
			np.savetxt(f,file_array)

#########################################"""
@numba.jit(nopython=True)
def _lorentz_kernel(omega,omega0,gamma):
	if omega==0 and omega0==0:
		return 1./(np.pi*gamma)
	else:
		return gamma*omega*omega/(
			np.pi*((gamma*omega)**2+omega**4
			-2.*(omega0*omega)**2+omega0**4)
		)

@numba.jit(nopython=True)
def _lorentz_kernel_qtb(omega,omega0,gamma,temperature):
	kT=temperature/3.15774e5
	
	if omega==0 and omega0==0:
		return 1./(np.pi*gamma)
	else:
		if omega==0:
			theta=kT
		else:
			theta=0.5*omega/kT/np.tanh(0.5*omega/kT)
		return theta*gamma*omega**2/(
			np.pi*((gamma*omega)**2+omega**4
			-2.*(omega0*omega)**2+omega0**4)
		)

@numba.jit(nopython=True)
def _compute_D(omega,omegaext,nom,nomext,dom,gamma,qtb=False,verbose=False,temperature=300.):
	D=np.zeros((nom,nom))
	K=np.zeros((nom,nomext))
	if qtb:
		for i in range(nom):
			om=omega[i]
			if verbose:
				print("D ",i+1)
			for j in range(nomext):
				x=omegaext[j]
				K[i,j]=_lorentz_kernel_qtb(x,om,gamma,temperature=temperature)
	else:
		for i in range(nom):
			om=omega[i]
			if verbose:
				print("D ",i+1)
			for j in range(nomext):
				x=omegaext[j]
				K[i,j]=_lorentz_kernel(x,om,gamma)

	D=np.dot(K,K.T)*dom
	if verbose:
		print("D done!")
	return D
	
@numba.jit(nopython=True)
def _build_K(omega,nom,dom,gamma,qtb=False,verbose=False,temperature=300.):
	#Kn=np.zeros((nom,nom),np.complex128)
	Kn=np.zeros((nom,nom))
	if qtb:
		for i in range(nom):
			om=omega[i]
			if verbose:
				print("K ",i+1)
			for j in range(nom):
				x=omega[j]
				Kn[i,j] = _lorentz_kernel_qtb(x,om,gamma,temperature=temperature)
	else:
		for i in range(nom):
			om=omega[i]
			if verbose:
				print("K ",i+1)
			for j in range(nom):
				x=omega[j]
				Kn[i,j] = _lorentz_kernel(x,om,gamma)

	if verbose:
		print("K done!")
	return Kn

@numba.jit(nopython=True)	
def _compute_h(spectrum,omega,nom,dom,K):
	return np.dot(K,spectrum)*dom
	
def _iterate_deconvolution(spectrum,D,h,K,Nit,omega,nom,dom,thr=1e-5,verbose=False):			
	f=np.copy(spectrum)
	fnext=np.zeros(nom,f.dtype)
	rn=np.zeros(Nit,f.dtype)
	ln=np.zeros(Nit,f.dtype)
	
	for it in range(Nit):		
		for i in range(nom):
			den=np.sum(f*D[i,:])*dom
			fnext[i]=f[i]*h[i]/den
		diff=np.sum(abs(fnext-f))/sum(abs(f))		
		fc=_compute_h(fnext,omega,nom,dom,K)		
		rn[it]=np.sum((fc-spectrum)**2)*dom
		d2=np.gradient(np.gradient(fnext,dom),dom)
		ln[it]=np.sum(d2**2)*dom
		if verbose:
			print("###############")
			print("iteration ",it+1)
			print("  Relative difference ",diff)
			print("  Relative error ",np.sum(abs(fc-spectrum))/np.sum(abs(spectrum)))
		f=np.copy(fnext)
		if diff <= thr:
			break
	return f,rn,ln
#################################################

#------------------------------------------------------------

def get_traj_from_file(filename,ncol=1,binary=False,rank=None,unit=1.,skip=0):
	if not binary:
		trajs=np.loadtxt(filename)
	else:
		trajType=np.dtype((float,ncol))
		trajs=np.fromfile(filename,dtype=trajType)
	
	if len(trajs.shape)==1:
		trajs=np.reshape(trajs,(len(trajs),1))
		
	if rank is None:
		return trajs[skip:]/unit
	else:
		return trajs[skip:,rank]/unit

def get_n_steps(pf,nsteps,ncol=1,rank=None,unit=1.,binary=False):
	if binary:
		trajType=np.dtype((float,ncol))
		trajs = np.fromfile(pf,dtype=trajType,count=nsteps)
	else:
		trajs = np.loadtxt(pf,max_rows=nsteps)

	if len(trajs.shape)==1:
		trajs=np.reshape(trajs,(len(trajs),1))
		
	if rank is None:
		return trajs[:]/unit
	else:
		return trajs[:,rank]/unit

class xyz_trajectory:
	def __init__(self,filename,dt=1.,skip=0,read=True):
		self.filename=filename
		self.atoms=[]
		self.dt=dt
		self.species=set(["*"])
		with open(filename,"r") as f:
			self.n_atoms=int(f.readline())
			for line in f:
				line=line.strip()
				#skip comments
				if line.startswith("#"):
					continue
				#split line
				parsed_line=line.split()
				#remove empty strings
				parsed_line=[x for x in parsed_line if x]
				#skip blank lines
				if not parsed_line:
					continue
				print(parsed_line[0])
				if len(parsed_line)==1:
					if int(parsed_line[0]) != self.n_atoms:
						print("Error: number of atoms cannot change!")
						raise ValueError
					else:
						break				
				self.atoms.append(Atom(parsed_line[0]))
				self.species.add(parsed_line[0])
		if read:
			self.read_trajectory(skip=skip)
	
	def read_trajectory(self,skip=0,sixcols=False):
		for atom in self.atoms:
			atom.traj=[]
		with open(self.filename,"r") as f:
			i=0
			for line in f:
				line=line.strip()
				#skip comments
				if line.startswith("#"):
					continue
				#split line
				parsed_line=line.split()
				#remove empty strings
				parsed_line=[x for x in parsed_line if x]
				#skip blank lines
				if not parsed_line:
					continue
				#print(parsed_line)
				if len(parsed_line)==1:
					if int(parsed_line[0]) != self.n_atoms:
						print("Error: number of atoms cannot change!")
						raise ValueError
					else:
						i=0
						continue
				atom=self.atoms[i]
				if atom.species != parsed_line[0]:
					print("Error: species of atoms cannot change!")
					raise ValueError
				atom.traj.append([	float(parsed_line[1]),
									float(parsed_line[2]),
									float(parsed_line[3])  ])
				if sixcols:
					atom.momentum.append([	float(parsed_line[4]),
									float(parsed_line[5]),
									float(parsed_line[6])  ])
				i+=1

		for atom in self.atoms:
			atom.traj=np.array(atom.traj[skip:])
		if sixcols:
			for atom in self.atoms:
				atom.momentum=np.array(atom.momentum[skip:])
		self.traj_length=self.atoms[0].traj.shape[0]
		print(str(self.traj_length)+" steps read.")
	
	def save_spectrum(self,species="*",filename="spectrum.out",unit=1.,cutoff=-1.,kubo=False, temperature=-1):
		if species not in self.species:
			print("species is not in species")
		trajsX=trajsY=trajsZ=[]
		for atom in self.atoms:
			if species=="*" or atom.species==species:
				trajsX.append(atom.traj[:,0])
				trajsY.append(atom.traj[:,1])
				trajsZ.append(atom.traj[:,2])

		specs=[]
		specs.append(mean_spectrum_from_trajs(trajsX,dt=self.dt))
		specs.append(mean_spectrum_from_trajs(trajsY,dt=self.dt))
		specs.append(mean_spectrum_from_trajs(trajsZ,dt=self.dt))
		if kubo:
			if temperature<0:
				print("Error: the temperature is not well defined ! Kubo inverse transform not applied.")
			else:
				for spec in specs:
					spec.inverse_kubo(temperature)
		self.spectra=specs
		write_spectra_to_file(specs,filename,unit=unit,cutoff=cutoff)
		return specs		


class Atom:
	def __init__(self,atom_species):
		self.species=str(atom_species)
		self.traj=[]
		self.momentum=[]
		
class LengthError(Exception):
	pass
