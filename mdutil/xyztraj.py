import numpy as np
import time
from .post_traj import Spectrum
from numba import njit

def clean_line(line):
  lineclean=line.strip()
  #skip comments
  if lineclean.startswith("#"):
    return None
  #split line
  parsed_line=lineclean.split()
  #remove empty strings
  parsed_line=[x for x in parsed_line if x]
  if parsed_line:
    return parsed_line
  else:
    return None

def get_xyz_info(filename):
  atoms=[]
  with open(filename,"r") as f:
    n_atoms=int(f.readline())
    for line in f:
      parsed_line=clean_line(line)
      if parsed_line is None:
        continue
      #print(parsed_line[0])
      if len(parsed_line)==1:
        if int(parsed_line[0]) != n_atoms:
          print("Error: number of atoms cannot change!")
          raise ValueError
        else:
          break	
      atoms.append(parsed_line[0])

  if len(atoms) != n_atoms:
    print("Error: number of atoms does not match data!")
    print(len(atoms),n_atoms)
    raise ValueError
	
  return atoms


def read_xyz_step(pf,n_atoms,positions):
  read_nat=False
  nat_read=0
  if positions.shape != (n_atoms,3):
    print("Error: incorrect position array shape!")
    print(positions.shape,(n_atoms,3))
    raise ValueError
  while True:
    line=pf.readline()
    if not line:
      return True
    parsed_line=clean_line(line)
    if parsed_line is None:
      continue
    if not read_nat:
      #read number of atoms
      nat_new=int(parsed_line[0])
      if nat_new != n_atoms:
        print("Error: number of atoms cannot change!")
        print(nat_new,n_atoms)
        raise ValueError
      read_nat=True
      continue
		
    for i in range(3):
      positions[nat_read,i]=float(parsed_line[i+1])
    nat_read+=1
    if(nat_read>=n_atoms):
      break
  return False	

def get_n_xyz_steps(pf,N,n_atoms):
  positions=np.zeros((n_atoms,3,N))
  eof=False
  for i in range(N):
    eof=read_xyz_step(pf,n_atoms,positions[:,:,i])
    if eof:
      return None
  return positions

def xyz_reader(filename,box_info=False,indexed=False
                  ,start=1, stop=-1,step=1,max_frames=None
                  ,stream=False,interval=2.,sleep=time.sleep):
  if stop>0 and start > stop: return
  pf=open(filename,"r")
  inside_frame=False
  nat_read=False
  iframe=0
  stride=step
  nframes=0
  if start>1 : print(f"skipping {start-1} frames")
  if not box_info: box=None
  while True:
    line=pf.readline()

    if not line:
      if stream:
        sleep(interval)
        continue
      elif inside_frame or nat_read:
        raise Exception("Error: premature end of file!")
      else:
        break
    
    line = line.strip()
    if line.startswith("#"): continue

    if not inside_frame:
      if not nat_read:
        nat=int(line.split()[0])
        nat_read=True
        iat=0
        inside_frame = not box_info
        xyz=np.zeros((nat,3))
        species=[]
        continue
      
      box=np.array(line.split(),dtype="float")
      inside_frame=True
      continue

    ls=line.split()
    iat,s = (int(ls[0]),1) if indexed else (iat+1,0)
    species.append(ls[s])
    xyz[iat-1,:] = np.array([ls[s+1],ls[s+2],ls[s+3]],dtype="float")
    if iat == nat:
      iframe+=1
      inside_frame=False
      nat_read=False
      if iframe < start: continue
      stride+=1
      if stride>=step:
        nframes+=1
        stride=0
        yield np.array(species),xyz,box
      if (max_frames is not None and nframes >= max_frames) \
          or (stop>0 and iframe >= stop):
        break

# def mean_spectrum(positions,dt):
#   N=positions.shape[2]
#   n_atoms=positions.shape[0]
#   n_dof=3*n_atoms
#   ft=np.fft.ifft(positions,2*N,axis=2,norm="ortho")
#   CC_mean=np.mean(np.real(ft*np.conj(ft)),axis=(0,1))
#   CC_mean*=2*dt
#   return pt.Spectrum(CC_mean,dt,np.pi/(N*dt))

def mean_spectrum(positions,dt,computeArray,*args,**kwargs):
  n_atoms=positions.shape[0]
  counter=0
  N=positions.shape[2]
  # spectrum_mean=pt.spectrum_from_traj(positions[0,0,:],dt=dt)
  # spectrum_mean.values[:]=0
  CC_mean=np.zeros((3,2*N))
  for i in range(n_atoms):
    if computeArray[i]:
      counter+=1
      for j in range(3):
        ft=np.fft.ifft(positions[i,j,:],2*N,norm="ortho")
        CC_mean[j,:]+=np.real(ft*np.conj(ft))
        # spectrum=pt.spectrum_from_traj(positions[i,j,:],dt=dt)
        # spectrum_mean.values+=spectrum.values/n_dof 
  # return spectrum_mean
  spectra=[]
  for j in range(3):
    spectra.append(Spectrum(CC_mean[j,:]*2*dt/counter,dt,np.pi/(N*dt)))
  return spectra

def mean_spectrum_dipole_moment(positions,dt,charges,*args,**kwargs):
  N=positions.shape[2]
  CC=np.zeros((3,2*N))
  spectra=[]
  for i in range(3):
    moment=np.sum(positions[:,i,:]*charges[:,np.newaxis],axis=0)  
    ft=np.fft.ifft(moment,2*N,norm="ortho")
    CC=np.real(ft*np.conj(ft))/3.
    spectra.append(Spectrum(CC*2*dt,dt,np.pi/(N*dt))) 
  return spectra

def mean_spectrum_xyz(filename,dt,block_size,charges=None,skip=0,species="*"):
  pf=open(filename,"r")
  atoms=get_xyz_info(filename)
  n_atoms=len(atoms)
  atoms_set=set(atoms)
  dipole_moment=False
  computeArray=[True for i in range(n_atoms)]
  print(f"n_atoms={n_atoms}, species={atoms_set}")
  if charges is not None:
    if charges.shape[0] != n_atoms:
      print("Error: number of atoms does not match charges!")
      raise ValueError
    print("Dipole moment spectrum.")
    dipole_moment=True
    compute_spectrum=mean_spectrum_dipole_moment
  else:
    if species!="*":
      computeArray=[atoms[i]==species for i in range(n_atoms)]
      #print(computeArray)
    compute_spectrum=mean_spectrum
  
  if skip > 0:
    skippos=np.zeros((n_atoms,3))
    print(f"Skipping {skip} frames.")
    for i in range(skip):
      read_xyz_step(pf,n_atoms,skippos)

  positions=get_n_xyz_steps(pf,block_size,n_atoms)
  if positions is None:
    print("Error: block_size > total n_steps")
    return None
  c=1
  print(f"block {c}")
  spectra_mean=compute_spectrum(positions,dt,computeArray=computeArray,charges=charges)
  # if dipole_moment:
  #   spectrum_mean=mean_spectrum_dipole_moment(positions,charges,dt)
  # else:
  #   spectrum_mean=mean_spectrum(positions,dt)
  
  while True:
    positions=get_n_xyz_steps(pf,block_size,n_atoms)
    if positions is None:
      break
    c+=1
    print(f"block {c}")
    spectra=compute_spectrum(positions,dt,computeArray=computeArray,charges=charges)
    for j in range(3):
      spectra_mean[j].values+=(spectra[j].values-spectra_mean[j].values)/c
  print(f"{c} blocks read.")
  return spectra_mean


def apply_pbc(vec,cell):
  cellinv = np.linalg.inv(cell)
  q = cellinv @ vec.T
  q-=np.rint(q)
  return (cell @ q).T

@njit
def apply_pbc_numba(vec,cell,cellinv):
  q = cellinv @ vec.T
  q-=np.rint(q)
  return (cell @ q).T

@njit
def pbc_distance(x1,x2,box=None):
  dx=x1-x2
  if box is not None: 
    dx[:] -= box[:3]*np.rint(dx[:]/box[:3])
  return np.linalg.norm(dx)

def get_pairs(species,sp1=None,sp2=None):
  #nat=len(species)
  #id1=np.flatnonzero(species==sp1) if sp1 is not None else np.arange(nat)
  #if sp2 != sp1:
  #  id2=np.flatnonzero(species==sp2) if sp2 is not None else np.arange(nat)
  #pairs=np.array([np.repeat(id1, len(id2)),np.tile(id2, len(id1))])
  #if sp1 is None or sp2 is None or sp1==sp2:
  #  pairs=pairs[:,pairs[0]!=pairs[1]]
  #return pairs
  #if sp1 is None and sp2 is not None: sp1=sp2
  #if sp2 is None and sp1 is not None: sp2=sp1
  if sp1 is None:
    if sp2 is None: return np.array(np.triu_indices(len(species),1))
    sp1=sp2
  id1=np.flatnonzero(species==sp1)
  if sp2==sp1 or sp2 is None:
    pairsloc=np.triu_indices(len(id1),1)
    return np.array([id1[pairsloc[0]],id1[pairsloc[1]]])  
  id2=np.flatnonzero(species==sp2)
  return np.array([np.repeat(id1, len(id2)),np.tile(id2, len(id1))])
    
@njit()
def radial_density_numba(species,xyz,box=None,rmax=10.0,dr=0.1,sp1=None,sp2=None):
  n_atoms=len(species)
  n_bins=int(rmax/dr)
  r=np.zeros(n_bins)
  n_pairs=0
  check_sp1=sp1 is not None
  check_sp2=sp2 is not None
  for i in range(n_atoms):
    if check_sp1 and species[i] != sp1: continue
    for j in range(n_atoms):
      if check_sp2 and species[j] != sp2: continue
      if i == j: continue
      n_pairs+=1
      rij=pbc_distance(xyz[i,:],xyz[j,:],box)
      if rij > rmax:
        continue
      rij_bin=int(rij/dr)
      r[rij_bin]+=1
  print(n_pairs)
  ii=np.arange(n_bins)
  expect = 4./3.*np.pi*dr**3*((ii+1)**3 - ii**3)
  if box is not None:
    volume=box[0]*box[1]*box[2]
    expect*=n_pairs/volume
  return r/expect

@njit()
def radial_densities(species,xyz,cell=None,rmax=10.0,dr=0.1):
  n_atoms=len(species)
  n_bins=int(rmax/dr)
  at_set=set(species)
  nspecies=len(at_set)
  sp_pairs={}
  c=0
  cellinv=np.linalg.inv(cell) if cell is not None else None
  for i in range(nspecies-1):
    for j in range(i+1,nspecies):
      sp_pairs[at_set[i]+at_set[j]]=c
      c+=1
  r=np.zeros((n_bins,c))
  n_pairs=0
  for i in range(n_atoms):
    for j in range(n_atoms):
      if i == j: continue
      scat=species[i]+species[j]
      if(scat not in sp_pairs): continue
      icol=sp_pairs[scat]
      n_pairs+=1
      vec = xyz[i,:]-xyz[j,:]
      if cell is not None: vec=apply_pbc_numba(vec,cell,cellinv)
      rij=np.linalg.norm(vec)
      if rij > rmax:
        continue
      rij_bin=int(rij/dr)
      r[rij_bin,icol]+=1
  print(n_pairs)
  bins=np.linspace(0,rmax,n_bins+1)
  expect = n_pairs*4./3.*np.pi*((bins[1:])**3 - bins[:-1]**3)
  if cell is not None:
    volume=np.dot(cell[:,0],np.cross(cell[:,1],cell[:,2]))
    expect/=volume
  return r/expect

@njit
def radial_histogram(xyz,id1,id2,rmax,dr,box):
  n_bins=int(rmax/dr)
  r=np.zeros(n_bins)
  n_pairs=0
  for i in id1:
    for j in id2:
      if i == j: continue
      n_pairs+=1
      rij=pbc_distance(xyz[i,:],xyz[j,:],box)
      if rij > rmax:
        continue
      rij_bin=int(rij/dr)
      r[rij_bin]+=1
  return r,n_pairs

def radial_density_smart(species,xyz,box=None,rmax=10.0,dr=0.1,sp1=None,sp2=None):
  n_atoms=len(species)
  id1=np.flatnonzero(species==sp1) if sp1 is not None else np.arange(n_atoms)
  id2=np.flatnonzero(species==sp2) if sp2 is not None else np.arange(n_atoms)
  r,n_pairs=radial_histogram(xyz,id1,id2,rmax,dr,box)
  #print(n_pairs)
  n_bins=len(r)
  bins=np.linspace(0,rmax,n_bins+1)
  expect = 4./3.*np.pi*((bins[1:])**3 - bins[:-1]**3)
  if box is not None:
    volume=box[0]*box[1]*box[2]
    expect*=n_pairs/volume
  return r/expect

def radial_density(species,xyz,cell=None,rmax=10.0,dr=0.1,sp1=None,sp2=None,pairs=None,timer=False):
  nat = len(species)
  if timer: 
    print(sp1,sp2)
    time0=time.time()
  if pairs is None:
    pairs=get_pairs(species,sp1,sp2)
  n_pairs=pairs.shape[1]
  mult = 1 if sp1!=sp2 else 2
  #print(mult*n_pairs,mult)
  if timer:
    print("pairs:",time.time()-time0)
    time0=time.time()
  vec=xyz[pairs[0]]-xyz[pairs[1]]
  if cell is not None:
    vec=apply_pbc(vec,cell)
  dist=np.linalg.norm(vec,axis=1)
  if timer: 
    print("dists:",time.time()-time0)
    time0=time.time()
  n_bins=int(rmax/dr)
  bins=np.linspace(0,rmax,n_bins+1)
  r,_=np.histogram(dist,bins=n_bins,range=(0,rmax))
  if timer: print("hist:",time.time()-time0)
  expect = n_pairs*4./3.*np.pi*((bins[1:])**3 - bins[:-1]**3)
  if cell is not None:
    volume=np.dot(cell[:,0],np.cross(cell[:,1],cell[:,2]))
    expect/=volume
  return r/expect
