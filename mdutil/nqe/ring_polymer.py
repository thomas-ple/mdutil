from re import U
import numpy as np
from ..atomic_units import au

class RingPolymer:
  potential=None
  
  def __init__(self,nbeads,temperature,M=None,mass=au.Mprot):
    self.M=M if M is not None  else nbeads
    assert self.M > 0 and self.M%2 != 0, "number of modes must be odd and > 0"
    self.k0=(self.M-1)//2
    self.iM=np.arange(-self.k0,self.k0+1)
    self.fluct=self.iM!=0
    
    assert mass>0, "mass must be > 0"
    self.mass=mass
    assert temperature>0, "temperature must be > 0"
    self.temperature=temperature
    self.kT=temperature/au.kelvin
    
    self.Q=np.zeros(self.M)
    self.P=np.zeros(self.M)
    self.F=np.zeros(self.M)
    self.Epot=0.
    self.Ekin=0.
    
    self.set_beads(nbeads)

  def __str__(self):
    if self.matsubara:
      return f"{self.M} modes Matsubara ring polymer at T={self.temperature}K"
    return f"{self.N} beads ring polymer at T={self.temperature}K"
  
  def set_beads(self,nbeads):
    assert nbeads>=self.M, "nbeads must be >= modes"
    self._nbeads=nbeads
    self.Tnm=RingPolymer.compute_Tnm(nbeads,self.M)
    
    if self.potential is not None:
      self.update_potential_forces()
    else:
      self.x=self.Tnm @ self.Q
      self.e=np.zeros(nbeads)
      self.dedx=np.zeros(nbeads)
    
    self.matsubara=nbeads>self.M
    if self.matsubara:
      self.omM=2.*np.pi*self.kT*self.iM
    else:
      self.omM=2.*nbeads*self.kT*np.sin(np.pi*self.iM/nbeads)
    self.omM2=self.omM**2
  
  def set_dynamics(self,dt,potential,gamma=0.,TRPMD=True,lambda_TRPMD=1.,integrator="BAOAB",dt_A=None,dt_B=None):
    assert dt>0, "dt must be > 0"
    self.dt=dt
    self.potential = potential
    self.integrator_name = integrator
    assert gamma>0, "gamma must be > 0"
    self.gamma=np.full(self.M,gamma)
    if TRPMD:
      assert lambda_TRPMD>0, "lambda_TRPMD must be > 0"
      absOm=np.abs(self.omM)
      sel=absOm > gamma
      self.gamma[sel] = lambda_TRPMD*absOm[sel]
    self.lgva = np.exp(-self.gamma*dt)
    self.lgvb = np.sqrt((1.-self.lgva**2)*self.mass*self.kT)
    
    self.dta=0.5*dt
    self.dtb=0.5*dt
    self.langevin_step = self.langevin_step_BAOAB
    if integrator=="OBABO": 
      self.dta=dt
      self.dtb=0.5*dt
      self.langevin_step = self.langevin_step_OBABO
    if dt_A is not None: 
      assert dt_A>0, "dt_A must be > 0"
      self.dta=dt_A
    if dt_B is not None:
      assert dt_B>0, "dt_B must be > 0"
      self.dtb=dt_B

    self.harmPP=np.cos(self.omM*self.dta)
    self.harmPQ=-self.mass*self.omM*np.sin(self.omM*self.dta)
    self.harmQQ=np.cos(self.omM*self.dta)
    self.harmQP=np.zeros_like(self.harmQQ)
    self.harmQP[self.fluct]=np.sin(self.omM[self.fluct]*self.dta)/(self.mass*self.omM[self.fluct])
    self.harmQP[self.k0]=self.dta/self.mass
      
  
  
  def sample_momentum_boltzmann(self):
    sig=np.sqrt(self.mass*self.kT)
    self.P[:] = np.random.normal(0.,sig,self.M)

  @staticmethod
  def compute_Tnm(N,M):
    k0=(M-1)//2
    Tnm=np.zeros((N,M))
    Tnm[:,k0]=1.
    dom=2.*np.pi*np.arange(1,N+1)/N
    for i in range(k0):
      ip=1+i
      Tnm[:,k0+ip]=np.sqrt(2.)*np.sin(dom*ip)
      Tnm[:,k0-ip]=np.sqrt(2.)*np.cos(dom*ip) 
    return Tnm

  @property
  def nbeads(self):
    return self._nbeads
  
  @nbeads.setter
  def nbeads(self,nbeads):
    self.set_beads(nbeads)
    
  
  @property
  def phase(self):
    return np.sum(self.Q[::-1]*self.P*self.omM) 

  @property
  def uncertainty(self):
    Pfluct=self.P[self.fluct]
    Qfluct=self.P[self.fluct]
    return np.sum(Pfluct**2)*np.sum(Qfluct**2)-np.sum(Pfluct*Qfluct)**2
  
  @property
  def kinetic_energy(self):
    Ekin = 0.5*np.sum(self.P**2/self.mass - self.mass*self.omM2*self.Q**2)
    self.Ekin=Ekin
    return Ekin
  
  @property
  def dtau(self):
    return 1./(self.kT*self._nbeads)
  
  @property
  def tau(self):
    return self.dtau*(np.arange(1,self._nbeads+1))
  
  @property
  def potential_path(self):
    return np.copy(self.e)
  
  @property
  def potential_energy(self):
    self.update_potential_forces()
    Epot = self.Epot
    return Epot
  
  def smooth_potential_path(self,M):
    assert M<=self.M, 'provided M must be <= polymer M'
    Q = np.zeros_like(self.Q)
    sel=np.abs(self.iM)<=(M-1)//2
    Q[sel] = self.Q[sel]
    x = self.Tnm @ Q
    e, dedx = self.potential(x)
    F = -(self.Tnm.T @ dedx)/self._nbeads
    return x,e,dedx,F

  def update_potential_forces(self):
    self.x = self.Tnm @ self.Q
    self.e, self.dedx = self.potential(self.x)
    self.Epot = np.sum(self.e)
    self.F = -(self.Tnm.T @ self.dedx)/self._nbeads
  
  def matsubara_verlet_step(self):
    self.P+=0.5*self.dt*self.F
    self.Q+=self.dt*self.P/self.mass
    self.update_potential_forces()
    self.P+=0.5*self.dt*self.F

  def apply_A(self):
    Pa=np.copy(self.P)
    Qa=np.copy(self.Q)
    self.P=Pa*self.harmPP+Qa*self.harmPQ
    self.Q=Qa*self.harmQQ+Pa*self.harmQP
  
  def apply_B(self,update_forces=True):
    if update_forces:
      self.update_potential_forces()
    self.P+=self.dtb*self.F
  
  def apply_O(self):
    self.P=self.P*self.lgva + np.random.normal(0.0,self.lgvb,self.M)
  
  def langevin_step_BAOAB(self):
    self.apply_B(update_forces=False)
    self.apply_A()
    self.apply_O()
    self.apply_A()
    self.apply_B(update_forces=True)
  
  def langevin_step_OBABO(self):
    self.apply_B(update_forces=False)
    self.apply_A()
    self.apply_B(update_forces=True)
    self.apply_O()
  
  def xsmooth(self,tau:float):
    return self.Q[self.k0]+np.sqrt(2.)*(
      np.sum(self.Q[:self.k0]*np.cos(self.omM[:self.k0]*tau))
      +np.sum(self.Q[self.k0+1:]*np.sin(self.omM[self.k0+1:]*tau))
    )
             