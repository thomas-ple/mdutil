import numpy as np
from ..atomic_units import au
from ..counter import Counter


class QTB:
  def __init__(self,dt,Tseg,omegacut,kT,gamma0,mass,hbar=1.,classical_kernel=False):
    self.dt=dt
    self.kT=kT
    self.omegacut=omegacut
    self.hbar = hbar
    self.mass=mass

    self.omegasmear = omegacut*0.04 
    self.nseg=int(Tseg/dt)
    self.Tseg=self.nseg*dt
    self.dom= 2.*np.pi/(3.*self.Tseg)
    self.nom=int(omegacut/self.dom)
    self.omega = self.dom*np.arange((3*self.nseg)//2+1)

    self.classical_kernel = classical_kernel

    if omegacut>=self.omega[-1] :
      raise ValueError("omegacut must be smaller than pi/dt")
    self.u = 0.5*hbar*np.abs(self.omega)/kT
    self.theta=kT*np.ones_like(self.omega)
    if hbar>0:
      self.theta[1:]*=self.u[1:]/np.tanh(self.u[1:])
    self.cutoff = 1./(1.+np.exp((np.abs(self.omega)-self.omegacut)/self.omegasmear))
    

    if gamma0>0.5*omegacut:
      raise ValueError("gamma0 must be much smaller than omegacut (at least half)")
    self.gamma0=gamma0
    self.gammar=gamma0*np.ones(self.nom)
    self.a1=np.exp(-gamma0*dt)
    self.OUcorr = ( ( 1. - 2.*self.a1*np.cos(self.omega*dt) + self.a1**2 )
                      / (dt**2*(gamma0**2+self.omega**2)) 
                  )
    self.white_noise = np.random.normal(0.,1.,(3*self.nseg))
    self.refresh_force()
    self.vel=np.zeros((self.nseg))
    self.counter = Counter(self.nseg)

  def ff_kernel(self):
    if self.classical_kernel:
      kernel = self.kT*np.ones_like(self.omega)*self.cutoff
    else:
      kernel = self.theta*self.cutoff*self.OUcorr
    kernel[:self.nom]*=self.gammar/self.gamma0
    return kernel*2*self.mass*self.gamma0/self.dt
  
  def refresh_force(self):
    self.white_noise[:2*self.nseg]=np.copy(self.white_noise[self.nseg:])
    self.white_noise[2*self.nseg:]=np.random.normal(0.,1.,self.nseg)
    s=np.fft.rfft(self.white_noise)*np.sqrt(self.ff_kernel())
    self.force = np.fft.irfft(s,3*self.nseg)[self.nseg:2*self.nseg]
  
  def apply_O(self,p):
    istep=self.counter.i()
    force = self.force[istep]
    self.vel[istep] = (p*np.sqrt(self.a1) + 0.5*self.dt*force)/self.mass
    self.counter.increment()
    if self.counter.is_reset_step():
      self.compute_spectra() 
      self.refresh_force()
    return p*self.a1 + self.dt*force
    
    
  def compute_spectra(self):
    sf=np.fft.rfft(self.force,3*self.nseg,norm="ortho")
    sv=np.fft.rfft(self.vel,3*self.nseg,norm="ortho")
    self.Cvv = (np.abs(sv)**2)[:self.nom]*self.dt
    self.Cvf = np.real(sv*np.conj(sf))[:self.nom]*self.dt
    self.Cff = (np.abs(sf)**2)[:self.nom]*self.dt

class AdQTB:
  def __init__(self,qtbs,Agamma=None,tau_ad=None,gammar=None,gammar_min=None):
    self.qtbs=qtbs
    self.nom=qtbs[0].nom
    self.omega=qtbs[0].omega[:self.nom]
    self.ndof=len(qtbs)
    self.classical_kernel = qtbs[0].classical_kernel

    if gammar_min is not None:
      self.gammar_min=gammar_min
    else:
      self.gammar_min=0.1*qtbs[0].gamma0

    if gammar is not None:
      self.gammar[:]=np.copy(gammar)
    else:
      self.gammar = np.copy(qtbs[0].gammar)
    for qtb in qtbs:
      qtb.gammar = self.gammar
    
    self.dFDT = np.zeros(self.nom)
    self.dFDT_avg = np.zeros_like(self.dFDT)
    self.mCvv_avg = np.zeros_like(self.dFDT)
    self.Cvfg_avg = np.zeros_like(self.dFDT)
    self.Cff_avg = np.zeros_like(self.dFDT)
    self.nadapt = 0

    self.Agamma = Agamma
    self.tau_ad = tau_ad
    self.do_adapt = (Agamma is not None) or (tau_ad is not None)
    if self.do_adapt:
      if Agamma is not None:
        assert Agamma >= 0, 'Agamma must be positive'
        self.a1 = Agamma*qtbs[0].Tseg*qtbs[0].gamma0
        self.update_gammar = self.update_gammar_simple
      elif tau_ad is not None:
        assert tau_ad > 0, 'tau_ad must be positive'
        self.update_gammar = self.update_gammar_ratio
        self.a1 = np.exp(-self.qtbs[0].Tseg/tau_ad)
        self.mCvv_m = np.zeros_like(self.dFDT)
        self.Cvf_m = np.zeros_like(self.dFDT)
  
  def compute_mean_spectra(self):
    self.mCvv = np.zeros_like(self.qtbs[0].Cvv)
    self.Cvf = np.zeros_like(self.qtbs[0].Cvf)
    self.Cff = np.zeros_like(self.qtbs[0].Cff)
    for qtb in self.qtbs:
      self.mCvv += qtb.mass*qtb.Cvv/self.ndof
      self.Cvf += qtb.Cvf/self.ndof
      self.Cff += qtb.Cff/self.ndof
    if self.classical_kernel:
      nom=self.qtbs[0].nom
      self.Cvf = self.Cvf*self.qtbs[0].theta[:nom]/self.qtbs[0].kT

  def compute_dFDT(self):
    self.dFDT = self.mCvv*self.gammar-self.Cvf
    return self.dFDT
  
  def compute_dFDT_PI(self,polymer):
    if self.nadapt==1:
      self.eF_avg = np.zeros_like(self.dFDT)
      self.eD_avg = np.zeros_like(self.dFDT)
    nom=self.qtbs[0].nom
    theta = self.qtbs[0].theta[:nom]/self.qtbs[0].kT
    omega = self.omega
    gamma0=self.qtbs[0].gamma0
    self.eF = np.zeros_like(self.dFDT)
    self.eD = np.zeros_like(self.dFDT)
    self.gF = np.zeros_like(self.dFDT)
    self.Tbeads = np.zeros_like(self.dFDT)
    for om in polymer.omM:
      omn = np.sqrt(omega**2+om**2)
      mCvv = np.interp(omn,omega,self.mCvv,right=0.,left=0.)
      Cvf = np.interp(omn,omega,self.Cvf,right=0.,left=0.)
      gammar = np.interp(omn,omega,self.gammar,right=gamma0,left=gamma0)
      sel=omn!=0
      self.Tbeads[sel] += (omega[sel]**2/omn[sel]**2)
      self.eD[sel]+=(omega[sel]**2/omn[sel]**2)*mCvv[sel]
      self.eF[sel]+=(omega[sel]**2/omn[sel]**2)*Cvf[sel] #/gammar[sel]
      self.gF[sel]+=(omega[sel]**2/omn[sel]**2)*gammar[sel]
    sel = self.gF!=0
    self.eF[sel] = self.eF[sel]
    self.dFDT = self.eD - self.eF
    self.eD_avg += (self.eD-self.eD_avg)/self.nadapt
    self.eF_avg += (self.eF-self.eF_avg)/self.nadapt
    return self.dFDT

  def update_gammar_simple(self):
    self.gammar-= self.a1*self.dFDT #np.linalg.norm(self.dFDT)
    self.propagate_gammar()
  
  def update_gammar_ratio(self):
    self.mCvv_m = self.mCvv_m*self.a1 + self.mCvv*(1.-self.a1)
    self.Cvf_m = self.Cvf_m*self.a1 + self.Cvf*(1.-self.a1)
    corr=1.0 #- self.a1**self.nadapt
    if self.nadapt*self.qtbs[0].Tseg>=self.tau_ad:
      self.gammar = self.Cvf_m/corr/(self.mCvv_m/corr + 1.e-8)
    self.propagate_gammar()
  
  def propagate_gammar(self):
    self.gammar[self.gammar<self.gammar_min] = self.gammar_min
    for qtb in self.qtbs:
      qtb.gammar = self.gammar
  
  def adapt_gammar(self,polymer=None):
    self.nadapt+=1
    self.compute_mean_spectra()
    if polymer is not None:
      self.compute_dFDT_PI(polymer)
    else:
      self.compute_dFDT()
    self.dFDT_avg += (self.dFDT-self.dFDT_avg)/self.nadapt
    self.mCvv_avg += (self.mCvv-self.mCvv_avg)/self.nadapt
    self.Cvfg_avg += (self.Cvf/self.gammar-self.Cvfg_avg)/self.nadapt
    self.Cff_avg += (self.Cff-self.Cff_avg)/self.nadapt
    if self.do_adapt: self.update_gammar()
